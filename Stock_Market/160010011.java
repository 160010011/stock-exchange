package main;

import java.util.ArrayList;

import customer.order;
import data.Read;

public class Main {

	/**
	 * @param args
	 */
	
	/************************************************************************
	 * Seller contains the list all the sellers who want to sell the items  *
	 * Buyer contains the list all the Buyers who want to buy the items		*
	 ************************************************************************/
	static ArrayList<order> seller = new ArrayList<>();
	static ArrayList<order> buyer = new ArrayList<>();
	
	public static void main(String[] args) {
		/*
		 * It reads the data from CSV File
		 */
		
		Read.readBooksFromCSV(args[0]);
		buynow();
		/*************************************************************************************
		 * You get the Options to choose from the List Displayed for a Particular Action	 *
		 * Enter the Number According to the action you want								 *
		 * 1. To Enter New data about buyer or seller										 *
		 * 2. To Buy from seller. This process all exchanges possible in one round			 *
		 * 3. This Displays the list Buyers with Price and Quantity in the Market 			 *
		 * 4. This Displays the list Sellers with Price and Quantity in the Market			 *
		 * 5. To Leave the program when you are done!										 *
		 * 																					 *
		 * You will be prompted again to choose from the options when the given instruction  *
		 * have been processed or you entered a wrong input ( Input Other than 1 to 5 ) 	 *
		 *************************************************************************************/
		/*
		while(true) {
			@SuppressWarnings("resource")
			Scanner input = new Scanner(System.in);
			System.out.println("Choose an option: ");
			System.out.println("1. Input data ");
			System.out.println("2. Buy");
			System.out.println("3. List of Buyers");
			System.out.println("4. List of Sellers");
			System.out.println("5. Exit");
	    	System.out.print("Enter an integer (1-5) : ");
	    	if(input.hasNextInt()) {
	    		int number = input.nextInt();
		    	switch(number){
		    		case 1 : Data.getdata();
		    			break;
		    		case 2 : buynow();
		    			break;
		    		case 3 : printConsumers(buyer);
		    			break;
		    		case 4 : printConsumers(seller);
		    			break;
		    		case 5 : System.exit(0);
		    			break;
		    		default : System.out.println("Choose correct option!");
		    			break;
		    	}
	    	}
		} 
		
		*/
	}
	
	/************************************************************
	 * This function is used to print List of buyers or sellers *
	 ************************************************************/
	
	/****************************************************************************
	 * @param buyer2															*
	 * It contains either the list of sellers or buyers depending upon which is *
	 * passed as an argument													*
	 ****************************************************************************/
	
	private static void printConsumers(ArrayList<order> buyer2) {
		System.out.println("Time  From  Stock  Quantity  Price");
		for (int i = 0; i < buyer2.size(); i++) {
			System.out.println(buyer2.get(i).getTime() + "     " + buyer2.get(i).getFrom() 
				   + "   " + buyer2.get(i).getStock() + "     " + buyer2.get(i).getQuantity() 
				   + "      " + buyer2.get(i).getPrice());
		}
	}

	/************************************************************
	 * This adds a new seller to the "seller" List				*
	 ************************************************************/
	
	/************************************************************
	 * @param addorder											*
	 * contains the order received as seller					*
	 ************************************************************/
	
	public static void addseller(order addorder) {
		seller.add(addorder);
	}
	
	/************************************************************
	 * This adds a new buyer to the "buyer" List				*
	 ************************************************************/
	
	/**
	 * @param addorder
	 * contains the order received as buyer
	 */
	public static void addbuyer(order addorder) {
		buyer.add(addorder);
	}
	
	/*************************************************************
	 * This Function performs the actual trade or stock exchange *
	 *************************************************************/
	private static void buynow() 
	{	
		for (int i = 0 ; i < buyer.size() ; i++) 
		{
			for (int j = 0; j < seller.size(); j++) 
			{	
				
			/*******************************************************************************
			 * Checks if the Price at which the buyer is willing to pay for the Stocks     *
			 * is more than the Price at which the seller is selling of a particular Stock *
			 * And It also checks if the buyer and seller are not the same.                *
			 * Since You cannot buy from yourself! 										   *
			 ******************************************************************************/
				if(buyer.get(i).getPrice() >= seller.get(j).getPrice() && 
				   buyer.get(i).getStock().equals(seller.get(j).getStock()) && 
				  !buyer.get(i).getFrom().equals(seller.get(j).getFrom())) 
				{
					
				/*****************************************************************
				 * qntb stores the Quantity of Stock the Buyer wants to Buy		 *
				 * qnts stores the Quantity of Stock available with that Seller	 *
				 *****************************************************************/
					int qntb = buyer.get(i).getQuantity();
					int qnts = seller.get(j).getQuantity();
					
				/*******************************************************************************
				 * If the Quantity of Stocks Available with the seller is less than the Demand *
				 * of the Buyer, The seller sells all the Stocks and His Business is Done	   *
				 * And Hence Leaves the Market After the Exchange							   *
				 * The Buyer then searches for another seller in the Market					   *
				 ******************************************************************************/
					if (qntb > qnts) 
					{	
						buyer.get(i).setQuantity(qntb-qnts);
						System.out.println(buyer.get(i).getFrom() + " bought " + qnts + " " + buyer.get(i).getStock() + " from " + seller.get(j).getFrom() + " at " + seller.get(j).getPrice());
						seller.remove(seller.get(j));
						i = i - 1;
						break;
					}
				/*******************************************************************************
				 * If the Demand of Stocks by the Buyer is less than the Availability of       *
				 * Stocks with the Seller, The Buyer Fulfills his Demand by buying all the 	   *
				 * required Stocks from that Seller And Leaves the Market After the Business   *
				 * is Done							   										   *	
				 ******************************************************************************/
					else if (qntb < qnts) 
					{
						seller.get(j).setQuantity(qnts-qntb);
						System.out.println(buyer.get(i).getFrom() + " bought " + qntb + " " + buyer.get(i).getStock() + " from " + seller.get(j).getFrom() + " at " + seller.get(j).getPrice());
						buyer.remove(buyer.get(i));
					}
				
				/*******************************************************************************
				 * If the Demand of Stocks by the Buyer is equal to the Stocks available with  *
				 * with the Seller, Both The Buyer and Seller Exchanges the Stocks fulfilling
				 * each others Demand and Supply.
				 * Both Leaves the Market After the Exchange is done.				   										   *	
				 ******************************************************************************/
					else 
					{
						System.out.println(buyer.get(i).getFrom() + " bought " + qnts + " " + buyer.get(i).getStock() + " from " + seller.get(j).getFrom() + " at " + seller.get(j).getPrice());
						buyer.remove(buyer.get(i));
						seller.remove(seller.get(j));
					}
				}
			}
		}
	}
}







package customer;

/**
 * @author Mayur
 *
 */
public class order {
	/**
	 * @param time 
	 * Stores the time at which the buyer or seller arrived
	 * @param from 
	 * Stores the name of the seller or the buyer
	 * @param stock
	 * Stores the name of the Stock
	 * @param quantity
	 * Stores the Quantity of the Specified Stock of supply or demand
	 * @param price
	 * Stores the cost or selling price of the Stock
	 */
	public order(int time, String from, String stock, int quantity, int price) {
		super();
		this.time = time;
		this.from = from;
		this.stock = stock;
		this.quantity = quantity;
		this.price = price;
	}
	public order() {
		// TODO Auto-generated constructor stub
	}
	int time;
	String from;
	String stock;
	int quantity;
	int price;
	/**
	 * @return the time
	 */
	public int getTime() {
		return time;
	}
	/**
	 * @param time the time to set
	 */
	public void setTime(int time) {
		this.time = time;
	}
	/**
	 * @return the from
	 */
	public String getFrom() {
		return from;
	}
	/**
	 * @param from the from to set
	 */
	public void setFrom(String from) {
		this.from = from;
	}
	/**
	 * @return the stock
	 */
	public String getStock() {
		return stock;
	}
	/**
	 * @param stock the stock to set
	 */
	public void setStock(String stock) {
		this.stock = stock;
	}
	/**
	 * @return the quantity
	 */
	public int getQuantity() {
		return quantity;
	}
	/**
	 * @param quantity the quantity to set
	 */
	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}
	/**
	 * @return the price
	 */
	public int getPrice() {
		return price;
	}
	/**
	 * @param price the price to set
	 */
	public void setPrice(int price) {
		this.price = price;
	}
}






package data;

import java.util.Scanner;

import customer.order;
import main.Main;

public class Data {
	
	public static int getdata() {
		@SuppressWarnings("resource")
		Scanner inputdata = new Scanner(System.in);
		
		/**************************************************************************************
		 * Input Data in the following Format with following the order of input				  *
		 * Input Should be comma Separated with 6 parameters 								  *
		 * Time, Type (B or S), From (Seller or Buyer name), Stock Name, Quantity, Price *
		 **************************************************************************************/
		
		System.out.println("Time ,Type ,From ,Stock ,Quantity ,Price");
		String data = null;
		if(inputdata.hasNextLine()) {
			data = inputdata.next();
		}
		String[] ordered_data = data.split(",", 6);
		if(ordered_data.length != 6){
			System.out.println("Wrong Input!, Please Check the Format of Input.");
			return 0;
		}
		order input = new order(Integer.parseInt(ordered_data[0].trim()),ordered_data[2].trim(),ordered_data[3].trim(),Integer.parseInt(ordered_data[4].trim()),Integer.parseInt(ordered_data[5].trim()));
		if (ordered_data[1].equals("B")) {
			Main.addbuyer(input);
		}
		else if (ordered_data[1].equals("S")) {
			Main.addseller(input);
		}
		else
			System.out.println("Wrong input!, Type should be either 'B' or 'S'.");
	//	inputdata.close();
		return 0;
	}
}



package data;

import java.io.BufferedReader;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import customer.order;
import main.Main;

public class Read {
	public static void readBooksFromCSV(String fileName) 
	{  
		Path pathToFile = Paths.get(fileName); 
		// create an instance of BufferedReader 
		// using try with resource, Java 7 feature to close resources 
		try (BufferedReader br = Files.newBufferedReader(pathToFile, StandardCharsets.US_ASCII)) 
		{ 
			// read the first line from the text file 
			String line = br.readLine(); // loop until all lines are read 
			while (line != null) 
			{ 
				// use string.split to load a string array with the values from 
				// each line of the file, using a comma as the delimiter 
				String[] ordered_data = line.split(",");
				order input = new order(Integer.parseInt(ordered_data[0]),ordered_data[2],ordered_data[3],Integer.parseInt(ordered_data[4]),Integer.parseInt(ordered_data[5]));
				if (ordered_data[1].equals("B")) 
				{
					Main.addbuyer(input);
				}
				else if (ordered_data[1].equals("S")) 
				{
					Main.addseller(input);
				}
				else
					System.out.println("Wrong input");
					// adding order into ArrayList 
					// read next line before looping 
					// if end of file reached, line would be null 
					line = br.readLine(); 
				} 
		} catch (IOException ioe) 
		{ 
			ioe.printStackTrace(); 
		}
	}
}


