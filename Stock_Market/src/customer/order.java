
package customer;

/**
 * @author Mayur
 *
 */
public class order {
	/**
	 * @param time 
	 * Stores the time at which the buyer or seller arrived
	 * @param from 
	 * Stores the name of the seller or the buyer
	 * @param stock
	 * Stores the name of the Stock
	 * @param quantity
	 * Stores the Quantity of the Specified Stock of supply or demand
	 * @param price
	 * Stores the cost or selling price of the Stock
	 */
	public order(int time, String from, String stock, int quantity, int price) {
		super();
		this.time = time;
		this.from = from;
		this.stock = stock;
		this.quantity = quantity;
		this.price = price;
	}
	public order() {
		// TODO Auto-generated constructor stub
	}
	int time;
	String from;
	String stock;
	int quantity;
	int price;
	/**
	 * @return the time
	 */
	public int getTime() {
		return time;
	}
	/**
	 * @param time the time to set
	 */
	public void setTime(int time) {
		this.time = time;
	}
	/**
	 * @return the from
	 */
	public String getFrom() {
		return from;
	}
	/**
	 * @param from the from to set
	 */
	public void setFrom(String from) {
		this.from = from;
	}
	/**
	 * @return the stock
	 */
	public String getStock() {
		return stock;
	}
	/**
	 * @param stock the stock to set
	 */
	public void setStock(String stock) {
		this.stock = stock;
	}
	/**
	 * @return the quantity
	 */
	public int getQuantity() {
		return quantity;
	}
	/**
	 * @param quantity the quantity to set
	 */
	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}
	/**
	 * @return the price
	 */
	public int getPrice() {
		return price;
	}
	/**
	 * @param price the price to set
	 */
	public void setPrice(int price) {
		this.price = price;
	}
}
