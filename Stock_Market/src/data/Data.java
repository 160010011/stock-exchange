package data;

import java.util.Scanner;

import customer.order;
import main.Main;

public class Data {
	
	public static int getdata() {
		@SuppressWarnings("resource")
		Scanner inputdata = new Scanner(System.in);
		
		/**************************************************************************************
		 * Input Data in the following Format with following the order of input				  *
		 * Input Should be comma Separated with 6 parameters 								  *
		 * Time, Type (B or S), From (Seller or Buyer name), Stock Name, Quantity, Price *
		 **************************************************************************************/
		
		System.out.println("Time ,Type ,From ,Stock ,Quantity ,Price");
		String data = null;
		if(inputdata.hasNextLine()) {
			data = inputdata.next();
		}
		String[] ordered_data = data.split(",", 6);
		if(ordered_data.length != 6){
			System.out.println("Wrong Input!, Please Check the Format of Input.");
			return 0;
		}
		order input = new order(Integer.parseInt(ordered_data[0].trim()),ordered_data[2].trim(),ordered_data[3].trim(),Integer.parseInt(ordered_data[4].trim()),Integer.parseInt(ordered_data[5].trim()));
		if (ordered_data[1].equals("B")) {
			Main.addbuyer(input);
		}
		else if (ordered_data[1].equals("S")) {
			Main.addseller(input);
		}
		else
			System.out.println("Wrong input!, Type should be either 'B' or 'S'.");
	//	inputdata.close();
		return 0;
	}
}
