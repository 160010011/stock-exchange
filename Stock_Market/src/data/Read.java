package data;

import java.io.BufferedReader;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import customer.order;
import main.Main;

public class Read {
	public static void readBooksFromCSV(String fileName) 
	{  
		Path pathToFile = Paths.get(fileName); 
		// create an instance of BufferedReader 
		// using try with resource, Java 7 feature to close resources 
		try (BufferedReader br = Files.newBufferedReader(pathToFile, StandardCharsets.US_ASCII)) 
		{ 
			// read the first line from the text file 
			String line = br.readLine(); // loop until all lines are read 
			while (line != null) 
			{ 
				// use string.split to load a string array with the values from 
				// each line of the file, using a comma as the delimiter 
				String[] ordered_data = line.split(",");
				order input = new order(Integer.parseInt(ordered_data[0]),ordered_data[2],ordered_data[3],Integer.parseInt(ordered_data[4]),Integer.parseInt(ordered_data[5]));
				if (ordered_data[1].equals("B")) 
				{
					Main.addbuyer(input);
				}
				else if (ordered_data[1].equals("S")) 
				{
					Main.addseller(input);
				}
				else
					System.out.println("Wrong input");
					// adding order into ArrayList 
					// read next line before looping 
					// if end of file reached, line would be null 
					line = br.readLine(); 
				} 
		} catch (IOException ioe) 
		{ 
			ioe.printStackTrace(); 
		}
	}
}
