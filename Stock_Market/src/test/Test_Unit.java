/**
 * 
 */
package test;

import static org.junit.Assert.*;

import org.junit.Test;

import customer.order;

/**
 * @author Mayur
 *
 */
public class Test_Unit {


	 @Test
	    public void testSetFrom() {
	        order card = new order();
	        String from = "myOrder";
	        card.setFrom(from);
	        assertEquals("Testing Source ", from, card.getFrom());
	    }
	 @Test
	    public void testSetPrice() {
	        order card = new order();
	        int price = 30;
	        card.setPrice(price);;
	        assertEquals("Testing Price ", price, card.getPrice());
	    }
	 @Test
	    public void testSetQuantity() {
	        order card = new order();
	        int quantity = 330;
	        card.setQuantity(quantity);;
	        assertEquals("Testing quantity ", quantity, card.getQuantity());
	    }
	 @Test
	    public void testSetStock() {
	        order card = new order();
	        String stock = "apple";
	        card.setStock(stock);;
	        assertEquals("Testing Stock ", stock, card.getStock());
	    }
	 @Test
	    public void testSetTime() {
	        order card = new order();
	        int time = 10;
	        card.setTime(time);;
	        assertEquals("Testing Time ", time, card.getTime());
	    }

}
